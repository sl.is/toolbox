<?php

$appDir = getenv('APP_DIR') ?: '/srv/codebase/app';

$finder = (new PhpCsFixer\Finder())
    ->ignoreDotFiles(false)
    ->ignoreVCSIgnored(true)
    ->in($appDir)
    ->exclude([
        'var',
        'vendor',
        'node_modules',
        'public',
        'bundles',
        'migrations',
        'config',
        'bin',
    ]);

return (new PhpCsFixer\Config())
    ->setRules([
        '@Symfony' => true,
        '@Symfony:risky' => true,
        '@PHP82Migration' => true,
        'explicit_string_variable' => true,
        'ordered_class_elements' => true,
        'global_namespace_import' => [
            'import_classes' => true,
            'import_constants' => false,
            'import_functions' => true,
        ],
    ])

    ->setUsingCache(false)
    ->setRiskyAllowed(true)
    ->setLineEnding("\n")
    ->setParallelConfig(PhpCsFixer\Runner\Parallel\ParallelConfigFactory::detect())
    ->setFinder($finder);
