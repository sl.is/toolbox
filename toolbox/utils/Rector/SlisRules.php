<?php

namespace Utils\Rector;

use Rector\Set\Contract\SetListInterface;

final class SlisRules implements SetListInterface
{
    public const ADD_API_PROPERTY = __DIR__.'/RuleSets/AddApiProperty.php';
    public const ADD_API_PROPERTY_ALWAYS = __DIR__.'/RuleSets/AddApiPropertyAlways.php';
    public const UP_TO_PHP82 = __DIR__.'/RuleSets/UpToPhp82.php';
    public const SKIP_RULES = __DIR__.'/RuleSets/SkipRules.php';
    public const PHP_UNIT = __DIR__.'/RuleSets/PhpUnitRules.php';
    public const SYMFONY = __DIR__.'/RuleSets/Symfony.php';
}
