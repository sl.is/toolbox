<?php

declare(strict_types=1);

namespace Utils\Rector\RuleSets;

use Rector\Config\RectorConfig;
use Utils\Rector\Rules\RectorAddApiProperty;

return static function (RectorConfig $rectorConfig): void {
    $rectorConfig->rule(RectorAddApiProperty::generateAlways(true));
};
