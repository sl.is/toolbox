<?php

namespace Utils\Rector\RuleSets;

use Rector\DeadCode\Rector\StaticCall\RemoveParentCallWithoutParentRector;
use Rector\Php81\Rector\ClassConst\FinalizePublicClassConstantRector;
use Rector\Php82\Rector\Class_\ReadOnlyClassRector;
use Rector\PHPUnit\Rector\ClassMethod\AddDoesNotPerformAssertionToNonAssertingTestRector;
use Rector\Symfony\Rector\MethodCall\ContainerGetToConstructorInjectionRector;

class SkipRules
{
    /**
     * @return class-string[]
     */
    public function index(): array
    {
        return [
            FinalizePublicClassConstantRector::class,
            /* class with non-readonly extended classes will also be readonly */
            ReadOnlyClassRector::class,
            /* to avoid that behaviour that the parent call will delete */
            RemoveParentCallWithoutParentRector::class,
            /* this rule will add DoesNotPerformAssertions - remove this when update to phpunit:10 */
            AddDoesNotPerformAssertionToNonAssertingTestRector::class,
            /* to avoid static::getContainer into __constructor in tests */
            ContainerGetToConstructorInjectionRector::class,
        ];
    }
}
