<?php

/*
 * How to use with rector.php
 *
 * implement the following calls within the rector function "return static function (RectorConfig $rectorConfig): void"
 *
 * Each of the following lines will do the same: Rector will not change existing examples. Use your prefered version
 * $rectorConfig->rule(\Utils\Rector\Rector\RectorAddApiProperty::generateAlways(false));
 * $rectorConfig->rule(\Utils\Rector\Rector\RectorAddApiProperty::generateAlways());
 * $rectorConfig->rule(\Utils\Rector\Rector\RectorAddApiProperty::class);
 *
 * In case you want to have new examples you have to use the following line
 * $rectorConfig->rule(\Utils\Rector\Rector\RectorAddApiProperty::generateAlways(true));
 */

namespace Utils\Rector\Rules;

use Exception;
use Faker\Factory;
use PHPParser\Comment\Doc;
use PhpParser\Node;
use PhpParser\Node\Arg;
use PhpParser\Node\Attribute;
use PhpParser\Node\AttributeGroup;
use PhpParser\Node\Expr\Array_;
use PhpParser\Node\Expr\ArrayItem;
use PhpParser\Node\Identifier;
use PhpParser\Node\Name;
use PhpParser\Node\Scalar\String_;
use PhpParser\Node\Stmt\Class_;
use PhpParser\Node\Stmt\Namespace_;
use PhpParser\Node\Stmt\Property;
use PhpParser\Node\Stmt\Use_;
use PhpParser\Node\Stmt\UseUse;
use Rector\Core\Rector\AbstractRector;
use Rector\Core\ValueObject\PhpVersion;
use Rector\VersionBonding\Contract\MinPhpVersionInterface;
use RuntimeException;
use Symplify\RuleDocGenerator\Exception\PoorDocumentationException;
use Symplify\RuleDocGenerator\ValueObject\RuleDefinition;

/*
 *  To implement a functionality to handle pre-configured property values see commit #0eed73f5 first
 *  (functions __construct, loadConfiguration, findContextData).
 */
class RectorAddApiProperty extends AbstractRector implements MinPhpVersionInterface
{
    private const APIPROPERTY = 'ApiProperty';
    private const TRIGGER = 'groups';
    private const APICONTEXT = 'openapiContext';
    private const LOCALE = 'de_DE';
    private const APIPLATFORM_NS = ['ApiPlatform', 'Metadata', 'ApiProperty'];

    private const SLIS_FAKER = 'slis-faker'; // must be lowercase
    private const X_FAKER = 'x-faker'; // must be lowercase
    private const EXAMPLE = 'example'; // must be lowercase

    private static bool $generateAlways = false;
    private static bool $checkNSFull = false;

    public static function generateAlways(bool $value = false): string
    {
        self::$generateAlways = $value;

        return self::class;
    }

    public function provideMinPhpVersion(): int
    {
        return PhpVersion::PHP_81;
    }

    /**
     * @throws PoorDocumentationException
     */
    public function getRuleDefinition(): RuleDefinition
    {
        return new RuleDefinition('Add API-Platform Descriptions.', []);
    }

    /**
     * @return array<class-string<Node>>
     */
    public function getNodeTypes(): array
    {
        return [Namespace_::class];
    }

    public function refactor(Node $node): ?Node
    {
        if ($node instanceof Namespace_) {
            $hasNSImported = false;
            $hasModified = false;
            foreach ($node->stmts as $stmt) {
                if ($stmt instanceof Use_) {
                    $hasNSImported = $hasNSImported || $this->checkAttribute($stmt);
                } elseif ($stmt instanceof Class_) {
                    $modArr = [];
                    foreach ($stmt->stmts as $subStmt) {
                        if ($subStmt instanceof Property) {
                            $result = $this->refactorProperty($subStmt);
                            if (null !== $result) {
                                $hasModified = true;
                                $modArr[] = $result;
                            } else {
                                $modArr[] = $subStmt;
                            }
                        } else {
                            $modArr[] = $subStmt;
                        }
                    }
                    $stmt->stmts = $modArr;
                }
            }
            if ((!$hasNSImported) && $hasModified) {
                $name = new Name($this->buildUseName());
                $use_use = new UseUse($name);
                $use = new Use_([$use_use]);
                array_unshift($node->stmts, $use);
            }

            return $node;
        }

        return null;
    }

    public function refactorProperty(Node $node): ?Node
    {
        if ($node instanceof Property) {
            $hasIdentifier = false;
            $current_args = null;
            $current_group_no = -1;

            $lower_trigger = strtolower(self::TRIGGER);
            $lower_property = strtolower(self::APIPROPERTY);

            $i = -1;
            foreach ($node->attrGroups as $attrGroup) {
                ++$i;
                foreach ($attrGroup as $attr) {
                    foreach ($attr as $item) {
                        if ($item instanceof Attribute) {
                            $parts = $item->name->parts;
                            $lower_name = strtolower($parts[\count($parts) - 1]);
                            if ($lower_name === $lower_trigger) {
                                $hasIdentifier = true;
                            }
                            if ($lower_name === $lower_property) {
                                $current_group_no = $i;
                                $current_args = $item->args;
                            }
                        }
                    }
                }
            }

            if ($hasIdentifier) {
                if ($current_group_no >= 0) {
                    unset($node->attrGroups[$current_group_no]);
                }
                $attributeGroups = $this->createApiProperty($current_args);
                $node->attrGroups = array_merge($node->attrGroups, $attributeGroups);

                return $node;
            }
        }

        return null;
    }

    private function buildDynString($item): string
    {
        if (\is_array($item)) {
            $value = '';
            foreach ($item as $key => $value) {
                if ('' !== $value) {
                    $value .= '; ';
                }
                if (null === $key) {
                    $value .= $value;
                } else {
                    $value .= $key.'='.$value;
                }
            }

            return $value;
        }

        return (string) $item;
    }

    private function detectInternalFunction($function): bool
    {
        return str_starts_with((string) $function, '#');
    }

    private function execInternalFunction(string $function, $items): string
    {
        $fct = strtolower(substr($function, 1));
        if ('const' === $fct) {
            return $this->buildDynString($items);
        } // else ... to be continued with more functions

        return substr((string) $function, 1);
    }

    private function buildArgArray(array $arr, array &$args): int
    {
        $args = [];
        $isAssoc = false;
        foreach ($arr as $arrItem) {
            if (null !== $arrItem->key) {
                $isAssoc = true;
                break;
            }
        }
        if ($isAssoc) {
            foreach ($arr as $arrItem) {
                if (!($arrItem->value instanceof String_)) { // can only process string => nothing to do
                    return -1;
                }
                $args[$arrItem->key->value] = $arrItem->value->value;
            }

            return 0;
        }
        foreach ($arr as $arrItem) {
            if (!($arrItem->value instanceof String_)) { // can only process string => nothing to do
                return -1;
            }
            $args[] = $arrItem->value->value;
        }

        return 1;
    }

    private function buildUseName(): string
    {
        $result = '';
        foreach (self::APIPLATFORM_NS as $iValue) {
            if ('' !== $result) {
                $result .= '\\';
            }
            $result .= $iValue;
        }

        return $result;
    }

    private function processFaker(array &$context_data, string $faker_key): string
    {
        $example = null;
        $comment = '';

        $faker_value = (($context_data[$faker_key]) instanceof String_) ? $context_data[$faker_key]->value : 'not_empty';
        if ('' === $faker_value) {
            $comment = 'replace x-faker to a valid format.';
        } elseif ((($context_data['example']->value ?? '') === '') || self::$generateAlways) {
            $faker_engine = Factory::create(self::LOCALE);
            $faker_data = $context_data[$faker_key];
            try {
                $argmode = 0;
                if ($faker_data instanceof Array_) {
                    if (('x-faker' === $faker_key) && (\count($faker_data->items) > 1)) {
                        throw new RuntimeException('Multiple items for \'x-faker\' found. Did you mean \'slis-faker\'?');
                    }
                    $example = '';
                    foreach ($faker_data->items as $faker_item) {
                        $function = $faker_item->key->value;
                        $items = ($faker_item->value instanceof String_) ? $faker_item->value->value : $faker_item->value->items;
                        if ($this->detectInternalFunction($function)) {
                            $example .= $this->execInternalFunction($function, $items);
                        } else {
                            $arguments = [];
                            $argmode = $this->buildArgArray($items, $arguments);
                            switch ($argmode) {
                                case 0:
                                    $example .= $faker_engine->$function(...$arguments);
                                    break;
                                case 1:
                                    $example .= $faker_engine->$function($arguments);
                                    break;
                            }
                        }
                    }
                } else {
                    if ($faker_data instanceof String_) { // can only process string
                        $function = $faker_data->value;
                        if ($this->detectInternalFunction($function)) {
                            $example = $this->execInternalFunction($function, null);
                        } else {
                            $example = $faker_engine->$function();
                        }
                    } else {
                        $argmode = -1;
                    }
                }
                if ($argmode >= 0) {
                    $context_data[self::EXAMPLE] = new String_($example);
                }
            } catch (Exception $exception) {
                $comment .= 'fix error: \''.$exception->getMessage().'\'.';
            }
        }

        return $comment;
    }

    /**
     * @return \AttributeGroup[]
     */
    private function createApiProperty(?array $current_args): array
    {
        //
        // make a copy of the data from apicontext
        $lower_context = strtolower(self::APICONTEXT);
        $context_data = [];
        $j = -1;
        foreach (($current_args ?? []) as $arg) {
            ++$j;
            if (($arg->name instanceof Identifier) && strtolower((string) $arg->name->name) === $lower_context) {
                foreach ($arg->value->items as $item) {
                    $context_data[$item->key->value] = $item->value;
                }
                unset($current_args[$j]);
                break;
            }
        }

        //
        // case distinction
        if (\array_key_exists(self::SLIS_FAKER, $context_data)) {
            $comment = $this->processFaker($context_data, self::SLIS_FAKER);
        } elseif (\array_key_exists(self::X_FAKER, $context_data)) {
            $comment = $this->processFaker($context_data, self::X_FAKER);
        } else {
            // We have to add an x-faker and a comment
            $comment = 'replace x-faker to a valid format.';
            $context_data[self::X_FAKER] = new String_('');
        }

        //
        // generate new attribute for rector
        $items = [];
        foreach ($context_data as $key => $value) {
            $key = (string) $key;
            $items[] = new ArrayItem(new ArrayItem($value), new ArrayItem(new String_($key)));
        }
        $current_args[] = new Arg(new Array_($items), false, false, [], new Identifier(self::APICONTEXT));
        $attribute = new Attribute(new Name(self::APIPROPERTY), $current_args);
        $attrGroup = new AttributeGroup([$attribute]);
        if ('' !== $comment) {
            $com = new Doc('/** @TODO - '.$comment.' */');
            $attrGroup->setDocComment($com);
        }

        return [$attrGroup];
    }

    private function checkAttribute(Use_ $use): bool
    {
        if (self::$checkNSFull) {
            $arr = $use->uses[0]->name->parts;
            $usecnt = \count(self::APIPLATFORM_NS);
            if (\count($arr) !== $usecnt) {
                return false;
            }
            for ($i = 0; $i < $usecnt; ++$i) {
                if ($arr[$i] !== self::APIPLATFORM_NS[$i]) {
                    return false;
                }
            }

            return true;
        }
        $cnt = \count($use->uses[0]->name->parts);
        if ($cnt > 0) {
            $found = $use->uses[0]->name->parts[$cnt - 1];
            $request = self::APIPLATFORM_NS[\count(self::APIPLATFORM_NS) - 1];

            return $found === $request;
        }

        return false;
    }
}
