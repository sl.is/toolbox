<?php

declare(strict_types=1);

use Rector\Config\RectorConfig;
use Rector\Core\ValueObject\PhpVersion;
use Utils\Rector\RuleSets\SkipRules;
use Utils\Rector\SlisRules;

return static function (RectorConfig $rectorConfig): void {
    $rectorConfig->importNames();
    $rectorConfig->importShortClasses();
    $rectorConfig->phpVersion(PhpVersion::PHP_82);

    $rectorConfig->sets([
        SlisRules::UP_TO_PHP82,
        SlisRules::ADD_API_PROPERTY,
        SlisRules::PHP_UNIT,
        SlisRules::SYMFONY,
    ]);

    $rectorConfig->skip((new SkipRules())->index());
};
