<?php

namespace App\Tests\rector\Fixture;

use Symfony\Component\Serializer\Annotation\Groups;

class TestEntity {

    #[Groups(['Entity:read', 'Entity:write'])]
    public string $todo = '';
}
?>
-----
<?php

namespace App\Tests\rector\Fixture;

use ApiPlatform\Metadata\ApiProperty;
use Symfony\Component\Serializer\Annotation\Groups;

class TestEntity {

    #[Groups(['Entity:read', 'Entity:write'])]
    /** @TODO - replace x-faker to a valid format. */
    #[ApiProperty(openapiContext: ['x-faker' => ''])]
    public string $todo = '';
}
?>
