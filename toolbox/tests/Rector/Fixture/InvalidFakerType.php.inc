<?php

namespace dummy;

use ApiPlatform\Metadata\ApiProperty;

class RectorInvalidFakerType {
    /*
     * PLEASE DO NOT MAKE ANY CHANGES! NETHER SPACES NOR NEW LINES!
     * A simple x-faker value should be processed
     */
    #[Groups(['Entity:read', 'Entity:write'])]
    #[ApiProperty(openapiContext: ['x-faker' => 'iri'])]
    public string $name;
}
?>
-----
<?php

namespace dummy;

use ApiPlatform\Metadata\ApiProperty;

class RectorInvalidFakerType {
    /*
     * PLEASE DO NOT MAKE ANY CHANGES! NETHER SPACES NOR NEW LINES!
     * A simple x-faker value should be processed
     */
    #[Groups(['Entity:read', 'Entity:write'])]
    /** @TODO - fix error: 'Unknown format "iri"'. */
    #[ApiProperty(openapiContext: ['x-faker' => 'iri'])]
    public string $name;
}
?>
