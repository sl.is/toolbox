<?php

namespace dummy;

use ApiPlatform\Metadata\ApiProperty;

class RectorForFakerTest {
    /*
     * PLEASE DO NOT MAKE ANY CHANGES! NETHER SPACES NOR NEW LINES!
     */
    public const constant = 'abc';

    #[Groups(['read'])]
    #[ApiProperty(openapiContext: ['x-faker' => ['randomElement' => ['string1', self::constant]], 'example' => ''])]
    public string $value1;

    #[Groups(['read'])]
    #[ApiProperty(openapiContext: ['x-faker' => ['randomElement' => [self::constant, 'string 2']], 'example' => ''])]
    public string $value2;

    #[Groups(['read'])]
    #[ApiProperty(openapiContext: ['slis-faker' => ['randomElement' => ['string1', self::constant]], 'example' => ''])]
    public string $value3;

    #[Groups(['read'])]
    #[ApiProperty(openapiContext: ['slis-faker' => ['randomElement' => [self::constant, 'string 2']], 'example' => ''])]
    public string $value4;
}
?>
-----
<?php

namespace dummy;

use ApiPlatform\Metadata\ApiProperty;

class RectorForFakerTest {
    /*
     * PLEASE DO NOT MAKE ANY CHANGES! NETHER SPACES NOR NEW LINES!
     */
    public const constant = 'abc';

    #[Groups(['read'])]
    #[ApiProperty(openapiContext: ['x-faker' => ['randomElement' => ['string1', self::constant]], 'example' => ''])]
    public string $value1;

    #[Groups(['read'])]
    #[ApiProperty(openapiContext: ['x-faker' => ['randomElement' => [self::constant, 'string 2']], 'example' => ''])]
    public string $value2;

    #[Groups(['read'])]
    #[ApiProperty(openapiContext: ['slis-faker' => ['randomElement' => ['string1', self::constant]], 'example' => ''])]
    public string $value3;

    #[Groups(['read'])]
    #[ApiProperty(openapiContext: ['slis-faker' => ['randomElement' => [self::constant, 'string 2']], 'example' => ''])]
    public string $value4;
}
?>
