<?php

namespace dummy;

use ApiPlatform\Metadata\ApiProperty;

class RectorForFakerTest {
    /*
     * PLEASE DO NOT MAKE ANY CHANGES! NETHER SPACES NOR NEW LINES!
     */
    #[Groups(['Entity:read', 'Entity:write'])]
    #[ApiProperty(openapiContext: ['slis-faker' => ['#const' => 'abc', '#const' => 'xyz']])]
    public string $value;
}
?>
-----
<?php

namespace dummy;

use ApiPlatform\Metadata\ApiProperty;

class RectorForFakerTest {
    /*
     * PLEASE DO NOT MAKE ANY CHANGES! NETHER SPACES NOR NEW LINES!
     */
    #[Groups(['Entity:read', 'Entity:write'])]
    #[ApiProperty(openapiContext: ['slis-faker' => ['#const' => 'abc', '#const' => 'xyz'], 'example' => 'abcxyz'])]
    public string $value;
}
?>
