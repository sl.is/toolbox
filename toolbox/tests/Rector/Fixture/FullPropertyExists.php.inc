<?php

namespace dummy;

use ApiPlatform\Metadata\ApiProperty;

class RectorForFakerTest {
    /*
     * PLEASE DO NOT MAKE ANY CHANGES! NETHER SPACES NOR NEW LINES!
     * The example of the ApiProperty is filled. So the faker should not change any value.
     */
    #[Groups(['Entity:read', 'Entity:write'])]
    #[ApiProperty(openapiContext: ['example' => 'filled', 'x-faker' => '#date'])]
    public string $date;
}
?>
-----
<?php

namespace dummy;

use ApiPlatform\Metadata\ApiProperty;

class RectorForFakerTest {
    /*
     * PLEASE DO NOT MAKE ANY CHANGES! NETHER SPACES NOR NEW LINES!
     * The example of the ApiProperty is filled. So the faker should not change any value.
     */
    #[Groups(['Entity:read', 'Entity:write'])]
    #[ApiProperty(openapiContext: ['example' => 'filled', 'x-faker' => '#date'])]
    public string $date;
}
?>
