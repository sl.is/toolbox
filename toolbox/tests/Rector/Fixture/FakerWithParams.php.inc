<?php

namespace dummy;

use ApiPlatform\Metadata\ApiProperty;

class RectorForFakerTest {
/*
 * PLEASE DO NOT MAKE ANY CHANGES! NETHER SPACES NOR NEW LINES!
 * The ApiProperty contains an x-faker value. So the example value should be filled with a new
 * value. (Of course a test value)
 */
#[Groups(['Entity:read', 'Entity:write'])]
#[ApiProperty(openapiContext: ['example' => '', 'x-faker' => ['#date' =>
    ['format' => 'd.m.Y',
      'max' => '-22 years']
]])]
public string $date;
}
?>
-----
<?php

namespace dummy;

use ApiPlatform\Metadata\ApiProperty;

class RectorForFakerTest {
/*
 * PLEASE DO NOT MAKE ANY CHANGES! NETHER SPACES NOR NEW LINES!
 * The ApiProperty contains an x-faker value. So the example value should be filled with a new
 * value. (Of course a test value)
 */
#[Groups(['Entity:read', 'Entity:write'])]
#[ApiProperty(openapiContext: ['example' => 'date', 'x-faker' => ['#date' =>
    ['format' => 'd.m.Y',
      'max' => '-22 years']
]])]
public string $date;
}
?>
