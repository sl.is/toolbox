<?php

namespace dummy;

use ApiPlatform\Metadata\ApiProperty;

class RectorForFakerTest {
    /*
     * PLEASE DO NOT MAKE ANY CHANGES! NETHER SPACES NOR NEW LINES!
     * There is an ApiProperty but with no x-faker option. An x-faker option should be set
     */
    #[Groups(['Entity:read', 'Entity:write'])]
    #[ApiProperty(identifier: false)]
    public string $date;
}
?>
-----
<?php

namespace dummy;

use ApiPlatform\Metadata\ApiProperty;

class RectorForFakerTest {
    /*
     * PLEASE DO NOT MAKE ANY CHANGES! NETHER SPACES NOR NEW LINES!
     * There is an ApiProperty but with no x-faker option. An x-faker option should be set
     */
    #[Groups(['Entity:read', 'Entity:write'])]
    /** @TODO - replace x-faker to a valid format. */
    #[ApiProperty(identifier: false, openapiContext: ['x-faker' => ''])]
    public string $date;
}
?>
