<?php

namespace dummy;

use ApiPlatform\Metadata\ApiProperty;

class RectorForFakerTest {
    /*
     * PLEASE DO NOT MAKE ANY CHANGES! NETHER SPACES NOR NEW LINES!
     */
    public const constant1 = 'abc';
    public const constant2 = 'xyz';

    #[Groups(['read'])]
    #[ApiProperty(openapiContext: ['x-faker' => ['randomElement' => [self::constant1, self::constant2]], 'example' => ''])]
    public string $value1;

    #[Groups(['read'])]
    #[ApiProperty(openapiContext: ['slis-faker' => ['randomElement' => [self::constant1, self::constant2]], 'example' => ''])]
    public string $value2;
}
?>
-----
<?php

namespace dummy;

use ApiPlatform\Metadata\ApiProperty;

class RectorForFakerTest {
    /*
     * PLEASE DO NOT MAKE ANY CHANGES! NETHER SPACES NOR NEW LINES!
     */
    public const constant1 = 'abc';
    public const constant2 = 'xyz';

    #[Groups(['read'])]
    #[ApiProperty(openapiContext: ['x-faker' => ['randomElement' => [self::constant1, self::constant2]], 'example' => ''])]
    public string $value1;

    #[Groups(['read'])]
    #[ApiProperty(openapiContext: ['slis-faker' => ['randomElement' => [self::constant1, self::constant2]], 'example' => ''])]
    public string $value2;
}
?>
