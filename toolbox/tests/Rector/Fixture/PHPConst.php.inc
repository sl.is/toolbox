<?php

namespace dummy;

use ApiPlatform\Metadata\ApiProperty;

class RectorForFakerTest {
    /*
     * PLEASE DO NOT MAKE ANY CHANGES! NETHER SPACES NOR NEW LINES!
     */
    public const constant = 'abc';

    #[Groups(['read'])]
    #[ApiProperty(openapiContext: ['x-faker' => self::constant, 'example' => ''])]
    public string $value1;

    #[Groups(['read'])]
    #[ApiProperty(openapiContext: ['slis-faker' => self::constant, 'example' => ''])]
    public string $value2;
}
?>
-----
<?php

namespace dummy;

use ApiPlatform\Metadata\ApiProperty;

class RectorForFakerTest {
    /*
     * PLEASE DO NOT MAKE ANY CHANGES! NETHER SPACES NOR NEW LINES!
     */
    public const constant = 'abc';

    #[Groups(['read'])]
    #[ApiProperty(openapiContext: ['x-faker' => self::constant, 'example' => ''])]
    public string $value1;

    #[Groups(['read'])]
    #[ApiProperty(openapiContext: ['slis-faker' => self::constant, 'example' => ''])]
    public string $value2;
}
?>
