<?php

namespace dummy;

use ApiPlatform\Metadata\ApiProperty;

class RectorForFakerTest {
    /*
     * PLEASE DO NOT MAKE ANY CHANGES! NETHER SPACES NOR NEW LINES!
     * The ApiProperty contains an x-faker value. So the example value should be filled with a new
     * value. (Of course a test value)
     */
    #[Groups(['Entity:read', 'Entity:write'])]
    #[ApiProperty(openapiContext: ['example' => '', 'x-faker' => ['#date' =>
          ['format' => 'd.m.Y',
           'min' => '-20 years'],
      '#const' => ' <==> ',
      '#date' =>
          ['format' => 'd.m.Y',
           'max' => '-20 years']
     ]])]
    public string $date;
}
?>
-----
<?php

namespace dummy;

use ApiPlatform\Metadata\ApiProperty;

class RectorForFakerTest {
    /*
     * PLEASE DO NOT MAKE ANY CHANGES! NETHER SPACES NOR NEW LINES!
     * The ApiProperty contains an x-faker value. So the example value should be filled with a new
     * value. (Of course a test value)
     */
    #[Groups(['Entity:read', 'Entity:write'])]
    /** @TODO - fix error: 'Multiple items for 'x-faker' found. Did you mean 'slis-faker'?'. */
    #[ApiProperty(openapiContext: ['example' => '', 'x-faker' => ['#date' =>
          ['format' => 'd.m.Y',
           'min' => '-20 years'],
      '#const' => ' <==> ',
      '#date' =>
          ['format' => 'd.m.Y',
           'max' => '-20 years']
     ]])]
    public string $date;
}
?>
