<?php

namespace dummy;

use ApiPlatform\Metadata\ApiProperty;

class RectorForFakerTest {
    /*
     * PLEASE DO NOT MAKE ANY CHANGES! NETHER SPACES NOR NEW LINES!
     * A simple x-faker value should be processed
     */
    #[Groups(['Entity:read', 'Entity:write'])]
    #[ApiProperty(openapiContext: ['example' => '', 'x-faker' => '#name'])]
    public string $name;
}
?>
-----
<?php

namespace dummy;

use ApiPlatform\Metadata\ApiProperty;

class RectorForFakerTest {
    /*
     * PLEASE DO NOT MAKE ANY CHANGES! NETHER SPACES NOR NEW LINES!
     * A simple x-faker value should be processed
     */
    #[Groups(['Entity:read', 'Entity:write'])]
    #[ApiProperty(openapiContext: ['example' => 'name', 'x-faker' => '#name'])]
    public string $name;
}
?>
