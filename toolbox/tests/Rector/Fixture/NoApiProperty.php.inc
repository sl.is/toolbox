<?php

namespace dummy;

use ApiPlatform\Metadata\ApiProperty;

class RectorForFakerTest {
    /*
     * PLEASE DO NOT MAKE ANY CHANGES! NETHER SPACES NOR NEW LINES!
     * The is no ApiProperty. An empty ApiProperty should be inserted.
     */
    #[Groups(['Entity:read', 'Entity:write'])]
    public string $date;
}
?>
-----
<?php

namespace dummy;

use ApiPlatform\Metadata\ApiProperty;

class RectorForFakerTest {
    /*
     * PLEASE DO NOT MAKE ANY CHANGES! NETHER SPACES NOR NEW LINES!
     * The is no ApiProperty. An empty ApiProperty should be inserted.
     */
    #[Groups(['Entity:read', 'Entity:write'])]
    /** @TODO - replace x-faker to a valid format. */
    #[ApiProperty(openapiContext: ['x-faker' => ''])]
    public string $date;
}
?>
