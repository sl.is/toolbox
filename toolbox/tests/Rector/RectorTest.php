<?php

namespace App\Tests\Rector;

include_once './vendor/autoload.php';

use Iterator;
use Rector\Testing\PHPUnit\AbstractRectorTestCase;

class RectorTest extends AbstractRectorTestCase
{
    /**
     * @dataProvider provideData()
     */
    public function test(string $file = ''): void
    {
        $this->doTestFile($file);
    }

    /**
     * @return Iterator<array<string>>
     */
    public function provideData(): Iterator
    {
        return $this->yieldFilesFromDirectory(__DIR__.'/Fixture');
    }

    public function provideConfigFilePath(): string
    {
        return __DIR__.'/config/rule.php';
    }
}
