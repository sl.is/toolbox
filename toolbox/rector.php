<?php

declare(strict_types=1);

include_once './vendor/autoload.php';

use Rector\Config\RectorConfig;
use Rector\Core\ValueObject\PhpVersion;
use Utils\Rector\RuleSets\SkipRules;
use Utils\Rector\SlisRules;

return static function (RectorConfig $rectorConfig): void {
    $rectorConfig->paths([
        __DIR__.'/codebase',
    ]);

    $rectorConfig->importNames();
    $rectorConfig->importShortClasses();
    $rectorConfig->phpVersion(PhpVersion::PHP_82);

    $rectorConfig->sets([
        SlisRules::UP_TO_PHP82,
        SlisRules::ADD_API_PROPERTY,
        SlisRules::PHP_UNIT,
        SlisRules::SYMFONY,
    ]);

    $rectorConfig->phpstanConfig(__DIR__.'/phpstan-dev.neon');

    $rectorConfig->skip(
        array_merge(
            (new SkipRules())->index(),
            [
                __DIR__.'/codebase/vendor',
                __DIR__.'/codebase/bin',
                __DIR__.'/codebase/var',
                __DIR__.'/codebase/public',
                __DIR__.'/codebase/assets',
                __DIR__.'/codebase/config',
                __DIR__.'/codebase/bundles',
                __DIR__.'/codebase/docker',
                __DIR__.'/codebase/migrations',
                __DIR__.'/codebase/src/Service/HereTodayGoneTomorrow',
                __DIR__.'/codebase/src/Service/Kim',
                __DIR__.'/codebase/src/Controller/Core/SecurityController.php',
            ]
        )
    );
};
