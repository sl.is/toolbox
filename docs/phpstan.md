# PHPStan

PHPStan will analyse our code for formatting errors and unknown functions and other awesome stuff like this.

## Runtime Structure

| Local path                          | Toolbox Path                        | Description                                                                 |
|-------------------------------------|-------------------------------------|-----------------------------------------------------------------------------|
| [ProjectRoot]                       | /srv/codebase                       | The whole project will be mapped inside the toolbox                         |
| [ProjectRoot]/app                   | /srv/codebase/app                   | The symfony application is here                                             |
| [ProjectRoot]/app/phpstan-dev.neon  | /srv/codebase/app/phpstan-dev.neon  | This file will be used to analyse the code in development, e.g. in phpstorm |
| [ProjectRoot]/app/phpstan-test.neon | /srv/codebase/app/phpstan-test.neon | This file will be used inside the test environment. e.g. in gitlab-ci       |

## Use PHPStan in PHPStorm
[Check this article](./implementation/phpstorm/phpstan.md)

## Use PHPStan in Gitlab CI
[Check this article](./implementation/gitlab-ci/phpstan.md)