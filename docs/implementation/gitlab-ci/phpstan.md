# PHPStan in Gitlab CI
To make use of this project implement our Toolbox into your Gitlab CI Pipelines!  
This will run phpstan with our phpstan configuration and check your codestyle.

```mermaid
sequenceDiagram
    actor Pipeline
    participant Toolbox
    participant Local-File-System
    participant Toolbox-File-System

    Pipeline->>Toolbox: Analyse Code with PHPStan
    Toolbox->>Local-File-System: Lade /srv/codebase/app/phpstan-dev.neon
    Local-File-System-->>Toolbox-File-System: Include /srv/codebase/app/phpstan-shared.neon
    Local-File-System-->>Toolbox-File-System: Include /srv/configs/phpstan/phpstan-dev.dist.neon
    Toolbox->>Pipeline: Report result
```

### Implement the pipeline
```yaml
include:
    -   project: 'sl.is/toolbox'
        ref: main
        file: 'shared/pipelines/phpstan.yml'
```

### Configure composer
To install private packages, please add the following variables to the gitlab ci  

| Key                    | Value                       |
|------------------------|-----------------------------|
| CI_COMPOSER_TOKEN_NAME | A valid Composer token name |
| CI_COMPOSER_TOKEN      | A valid Composer token      |

## Rector

// TBD
