# Run PHP CS Fixer manually
Sometimes you might want to fix your whole code.  
Please use the following command to run php-cs-fixer with our config file in the whole project.
```bash
docker run -v ${PWD}:/srv/codebase slisio/toolbox php-cs-fixer
```

## Custom App Directory
If your application is in another directory than /srv/codebase/app, than you need to define it!

```bash
docker run -v ${PWD}:/srv/codebase -v ${PWD}/custom-sources:/srv/codebase/app slisio/toolbox php-cs-fixer
``` 