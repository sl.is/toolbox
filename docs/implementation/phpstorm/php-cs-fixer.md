# PHP CS Fixer
Make sure that you have done the [General implementation](./general.md)

## How to
![Step](../../_files/php-cs-fixer-1.png)  
![Step](../../_files/php-cs-fixer-2.png)  
![Step](../../_files/php-cs-fixer-3.png)  
![Step](../../_files/php-cs-fixer-4.png)  
![Step](../../_files/php-cs-fixer-5.png)  