# PHP Stan
Make sure that you have done the [General implementation](./general.md)
```mermaid
sequenceDiagram
    actor User
    participant PHPStorm
    participant Toolbox
    participant Local-File-System
    participant Toolbox-File-System

    User->>PHPStorm: Analyse Code with PHPStan
    PHPStorm->>Toolbox: Analyse File X
    Toolbox->>Local-File-System: Lade /srv/codebase/app/phpstan-dev.neon
    Local-File-System-->>Toolbox-File-System: Include /srv/codebase/app/phpstan-shared.neon
    Local-File-System-->>Toolbox-File-System: Include /srv/configs/phpstan/phpstan-dev.dist.neon
    Toolbox->>PHPStorm: Report result
    PHPStorm->>User: Report result
```

## How to setup the project
- Add the files from shared/template/app/ to the project directory


## How to use in phpstorm
![Step](../../_files/phpstan-1.png)  
![Step](../../_files/phpstan-2.png)  
![Step](../../_files/phpstan-3.png)  
![Step](../../_files/phpstan-4.png)  
![Step](../../_files/phpstan-5.png)  