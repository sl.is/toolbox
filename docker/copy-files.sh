#!/bin/sh

BASE_DIR="/srv/codebase"
APP_DIR=""

echo "copy git hooks"
if [ -d "${BASE_DIR}/.git" ]; then
    cp -f git-hooks/* "${BASE_DIR}/.git/hooks/"
    echo "Git hooks copied and overwritten successfully."
else
    echo ".git directory does not exist."
fi
