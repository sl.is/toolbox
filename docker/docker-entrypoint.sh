#!/bin/sh
set -e

export CONFIG_DIR=${CONFIG_DIR:-/srv/configs}
export PATH="${PATH}:/srv/toolbox/vendor/bin:/srv/php-cs-fixer/vendor/bin"
export APP_DIR=${APP_DIR:-/srv/codebase/app/}
export APP_ENV=${APP_ENV:-dev}

checkPhpStanNeon()
{
    if [ ! -f "$APP_DIR/phpstan-$APP_ENV.neon" ]; then
        echo "phpstan-$APP_ENV.neon not found in $APP_DIR Please create it with the following content"
        echo "includes:"
        echo "    - /srv/configs/phpstan/phpstan.neon"
        exit 1
    fi
}

if [ "$0" = "phpstan" ]; then
    # docker run slisio/toolbox phpstan [any parameter]
    checkPhpStanNeon
fi

if [ "$@" = "phpstan" ]; then
    # docker run slisio/toolbox phpstan
    checkPhpStanNeon

    cd $APP_DIR

    # If we are in a symfony application, lets clear and warmup the cache
    if [ -f "$APP_DIR/bin/console" ]; then
        bin/console cache:pool:prune
        bin/console cache:warmup
    fi

    phpstan analyse \
        --memory-limit=2G \
        -c $APP_DIR/phpstan-$APP_ENV.neon \
        --autoload-file=$APP_DIR/vendor/autoload.php
elif [ "$@" = "phpstan-raw" ]; then
    # docker run slisio/toolbox phpstan
    checkPhpStanNeon

    cd $APP_DIR

    # If we are in a symfony application, lets clear and warmup the cache
    if [ -f "$APP_DIR/bin/console" ]; then
        bin/console cache:pool:prune
        bin/console cache:warmup
    fi

    phpstan analyse \
        --memory-limit=2G \
        -c "$APP_DIR/phpstan-$APP_ENV.neon" \
        --autoload-file="$APP_DIR/vendor/autoload.php" \
        --no-progress \
        --error-format=raw
elif [ "$@" = "phpstan-ci" ]; then
    # docker run slisio/toolbox phpstan-ci
    export APP_ENV=test
    checkPhpStanNeon

    cd $APP_DIR
    # If we are in a symfony application, lets clear and warmup the cache
    if [ -f "$APP_DIR/bin/console" ]; then
        bin/console cache:pool:prune
        bin/console cache:warmup
    fi
    phpstan analyse \
        --memory-limit=2G \
        -c $APP_DIR/phpstan-$APP_ENV.neon \
        --autoload-file=$APP_DIR/vendor/autoload.php \
        --no-progress \
        --error-format gitlab > /ci/phpstan.json
elif [ "$@" = "php-cs-fixer" ]; then
    cd $APP_DIR
    /srv/php-cs-fixer/vendor/bin/php-cs-fixer fix --config=${CONFIG_DIR}/php-cs-fixer/.php-cs-fixer.dist.php
elif [ "$@" = "php-cs-fixer-ci" ]; then
    cd $APP_DIR
    /srv/php-cs-fixer/vendor/bin/php-cs-fixer fix --dry-run --format=gitlab --config=${CONFIG_DIR}/php-cs-fixer/.php-cs-fixer.dist.php --using-cache=no > /ci/php-cs-fixer.json || true
    if [ "$(cat /ci/php-cs-fixer.json)" != "[]" ]; then echo "Failed!"; cat /ci/php-cs-fixer.json; exit 1; fi
else
    echo "Running toolbox command: $@"
    $@
fi
