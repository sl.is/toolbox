# slis toolbox

## Implementation
- PHPStorm
    - [PHP CS Fixer configuration](./docs/implementation/phpstorm/php-cs-fixer.md)
    - [PHPStan configuration](./docs/implementation/phpstorm/phpstan.md) 
- Gitlab-CI
    - [PHPStan](./docs/implementation/gitlab-ci/phpstan.md)

## Our Toolbox in detail
The idea is, that you have a local project.
This local project will format everything using the toolbox

### File structure
- /srv
  - /srv/codebase: You will always map the project directory here
  - /srv/toolbox: Everything toolbox related

## Entrypoint
| Command    | Description                                                                                         |
|------------|-----------------------------------------------------------------------------------------------------|
| phpstan    | Runs phpstan in the /srv/codebase/app directory                                                     |
| phpstan-ci | Runs phpstan in the /srv/codebase/app directory for gitlab and saved the result to /ci/phpstan.json |