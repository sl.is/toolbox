#!/usr/bin/make

SHELL = /bin/sh
PWD := $(shell pwd)
CURRENT_UID := $(shell id -u)
CURRENT_GID := $(shell id -g)

VERSION := latest
ifneq ($(CI_COMMIT_BRANCH), main)
	VERSION = latest
endif

ifndef IMAGE_NAME
	IMAGE_NAME = slisio/toolbox
endif

build:
	docker build -t ${IMAGE_NAME} .

push:
	docker push ${IMAGE_NAME}

composer/%:
	@docker run \
		-ti --rm \
		-v ${PWD}/toolbox:/app \
		-w /app \
		-u ${CURRENT_UID}:${CURRENT_GID} \
		composer:2 composer $*
