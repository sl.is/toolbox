FROM php:8.2-alpine

RUN apk --update add make
RUN apk add gnupg
RUN apk --update add icu-dev \
    && docker-php-ext-configure intl \
    && docker-php-ext-install intl

# Node
RUN set -eux; \
    apk add --update npm

COPY --from=composer:2 /usr/bin/composer /usr/bin/composer

ENV COMPOSER_ALLOW_SUPERUSER=1
ENV COMPOSER_MEMORY_LIMIT=2G
ENV PATH="${PATH}:/srv/toolbox/vendor/bin"

WORKDIR /srv
COPY . /srv

# Install PHP CS Fixer into another directory
RUN set -aux; \
   mkdir -p php-cs-fixer; \
   composer require --working-dir=./php-cs-fixer friendsofphp/php-cs-fixer

RUN set -eux; \
    cd toolbox; \
	composer install --prefer-dist --no-dev --no-scripts --no-progress; \
	composer clear-cache

COPY docker/docker-entrypoint.sh /usr/local/bin/docker-entrypoint
RUN chmod +x /usr/local/bin/docker-entrypoint
COPY docker/php/memory_limit.ini /usr/local/etc/php/conf.d/zz-memory_limit.ini
ENTRYPOINT ["docker-entrypoint"]
